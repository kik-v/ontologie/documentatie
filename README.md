### onz prefixes zijn als volgt
|prefix|url|
|-|-|
|onz-g|https://purl.org/ozo/onz-g#|
|onz-pers|https://purl.org/ozo/onz-pers#|
|onz-fin|https://purl.org/ozo/onz-fin#|
|onz-zorg|https://purl.org/ozo/onz-zorg#|
|onz-org|https://purl.org/ozo/onz-org#|

### Versie annotatie
Versies van RDF bestanden worden nooit in uri’s opgenomen maar als een eigenschap van de ontologie middels owl:versionIRI  en owl:versionInfo .
Van geïmporteerde vocabulaires wordt met een locale versie gewerkt zodat externe wijzigingen niet tot interne problemen zal leiden. Dit geldt b.v. voor de TIME ontologie. Nieuwe versies worden eerst getest en daarna deployed.

### Imports en hergebruik
Binnen vph wordt standaard de onz-g ontologie importeerd. Modules importeren elkaar niet maar geven bij hergebruik aan waar de class of property gedefinieerd is middels rdfs:isDefinedBy

### purl.org settings
Onderstaande settings zorgen ervoor dat de ontologieën en de documentatie op een permanente URL te vinden zijn, onafhankelijk van de locatie van de bron.

|name|type|targt|
|-|-|-|
|/ozo/onz-g.owl|302 Found|https://gitlab.com/kik-v/ontologie/source/-/raw/main/onz-g.owl|
|/ozo/onz-g|302 Found|https://kik-v.gitlab.io/ontologie/onz-g/|
|/ozo/onz-g/|partial|https://kik-v.gitlab.io/ontologie/onz-g/index.html#|
|/ozo/onz-pers.owl|302 Found|https://gitlab.com/kik-v/ontologie/source/-/raw/main/onz-pers.owl|
|/ozo/onz-pers|302 Found|https://kik-v.gitlab.io/ontologie/onz-pers/|
|/ozo/onz-pers/|partial|https://kik-v.gitlab.io/ontologie/onz-pers/index.html#|
|/ozo/onz-fin.owl|302 Found|https://gitlab.com/kik-v/ontologie/source/-/raw/main/onz-fin.owl|
|/ozo/onz-fin|302 Found|https://kik-v.gitlab.io/ontologie/onz-fin/|
|/ozo/onz-fin/|partial|https://kik-v.gitlab.io/ontologie/onz-fin/index.html#|
|/ozo/onz-org.owl|302 Found|https://gitlab.com/kik-v/ontologie/source/-/raw/main/onz-org.owl|
|/ozo/onz-org|302 Found|https://kik-v.gitlab.io/ontologie/onz-org/|
|/ozo/onz-org/|partial|https://kik-v.gitlab.io/ontologie/onz-org/index.html#|
|/ozo/onz-zorg.owl|302 Found|https://gitlab.com/kik-v/ontologie/source/-/raw/main/onz-zorg.owl|
|/ozo/onz-zorg|302 Found|https://kik-v.gitlab.io/ontologie/onz-zorg/|
|/ozo/onz-zorg/|partial|https://kik-v.gitlab.io/ontologie/onz-zorg/index.html#|

### Van brondata naar RDF m.b.v. ontologie
[Hier](./Source2RDF/README.md) vind u meer informatie over hoe de ontolgie gebruikt dient te worden in het transformeren van brondata naar RDF triples.