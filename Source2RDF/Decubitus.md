# RDF triples m.b.t. Decubitus
In dit document wordt aan de hand van voorbeelddata duidelijk gemaakt hoe RDF triples gemaakt kunnen worden over het onderwerp decubitus.
## Ontologie
![Decubituswond](./img/Decubituswond_protege.png "Decubituswond in de ontologie")
In de ontologie staat het concept [DecubitusWond](http://purl.org/ozo/vph-zorg#DecubitusWond). Naast zaken als de definitie en een verwijzing naar de equivalente term volgens Snomed, staan er de volgende axioma's. 
SubClass Of:
* hasQuality only (MedicalRatingQuality and hasQualityValue only Decubituscategorie)
* hersteldatum some xsd:date
* inheresIn some Human
* Lesion
Hieronder staat de betekenis van deze axioma's kort toegelicht.
### hasQuality
Het eerste axioma volgt het patroon van een quality-region. Dat is een manier om eigenschappen van iets weer te geven in triples. Dit patroon wordt op meerdere plekken in de ontologie gebruikt.

[hasQuality](http://purl.org/ozo/vph-g#hasQuality) is een objectproperty met als range een [Quality](http://purl.org/ozo/vph-g#Quality). Qualities kun je zien als eigenschappen van dingen die je kunt observeren/meten.
Het axioma zegt dat een Decubituswond een kwaliteit heeft van het type [MedicalRatingQuality](http://purl.org/ozo/vph-g#MedicalRatingQuality). Deze heeft een kwaliteitswaarde ([hasQualityValue](http://purl.org/ozo/vph-g:hasQualityValue)) uit de klasse [Decubituscategorie](http://purl.org/ozo/vph-zorg#DecubitusCategorie). Dat is een klasse met de vier categorieën die gebruikt worden om de ernst van een decubituswond aan te duiden.

In gewone taal: Een decubituswond heeft een kwaliteit en die kwaliteit druk je uit aan de hand van de schaal decubituscategorie. Aan die kwaliteit kun je overigens een datum 'hangen' om aan te geven op welke moment deze kwaliteit is vastgesteld. Daarover later meer.
### hersteldatum
De datum waarop de wond hersteld is, wordt rechtstreeks aan de wond gekoppeld via de dataproperty [herstelDatum](http://purl.org/ozo/vph-zorg#herstelDatum)
### inheresIn
Het twee axioma stelt dat een decubituswond [inheres in](http://purl.org/ozo/vph-g#inheresIn) iemand van het type [Human](http://purl.org/ozo/vph-g#Human). Dat betekent dat een decubituswond een kwaliteit is (want het domein van de property inheresIn is een Quality) die voorkomt in mensen. Het omgekeerde geldt ook: een mens kan als kwaliteit een decubituswond hebben. Zoals hierboven duidelijk is geworden kan die wond ook weer een kwaliteit hebben: de ernst van de wond. De objectproperty [inheres in](http://purl.org/ozo/vph-g#inheresIn) is dan ook de omgekeerde property van [hasQuality](http://purl.org/ozo/vph-g#hasQuality)
### Lesion
Dit axioma betekent simpelweg dat het concept [DecubitusWond](http://purl.org/ozo/vph-zorg#DecubitusWond) een verbijzondering is van het concept [Lesion](http://purl.org/ozo/vph-g#Lesion)(een abnormale morfologie in het weefsel van een organisme). Anders verwoord: de klasse 'DecubitusWond' is een subklasse van 'Lesion'.
## RDF
Met behulp van bovenstaande informatie uit de ontologie, wordt op anderstaande manier de informatie over een decubituswond en de ernst ervan weergegeven in RDF triples.
![RDF](./img/Decubitus_rdf.png "Decubituswond en ernst in RDF")

### Wond
Van een wond wordt aangegeven dat het om een decubituswond gaat, door de klasse [DecubitusWond](http://purl.org/ozo/vph-zorg#DecubitusWond) te instantiëren. Dat wil zeggen: er wordt een individu aangemaakt, in de klasse DecubitusWond. Om een individu aan te maken is een Unique Resource Identiefier (URI) nodig. Deze heeft de vorm van een URL en dient globaal uniek te zijn.

Advies om hiervoor een patroon te gebruiken waarbij het begin bestaat uit een domeinnaam die uniek is voor het betreffende verpleeghuis en waarover dat verpleeghuis (of een leverancier) het beheer kan voeren, om te garanderen dat er een unieke identifier ontstaat (1). Vervolgens kan het voor leesbaarheid (in het uniek zijn...) een categorie worden toegevoegd, bijvoorbeeld 'decubituswond'(2), om af te sluiten met een identifier voor de specifieke wond (3). Dit kan in sommige gevallen overeen komen met een identiefier die gebruikt wordt in een tabel met wonden. Ook kan een rijnummer in een tabel worden gebruikt. Indien er meerdere graden zijn geconstateerd is het uiteraard van belang dat deze wel naar dezelfde wond verwijzen. Dus bij rijnummers is dat een aandachtspunt!

Voorbeeld:
1. http://data.zorgaanbiederA.nl/
2. decubituswond/
3. 12h3fgs

Resultaat: http://data.zorgaanbiederA.nl/decubituswond/12h3fgs

Het is niet noodzakelijk dat het volgen van deze link een leesbare webpagina (voor mens danwel voor machine) oplevert.
### Persoon
De persoon die de wond heeft dient ook een URI te krijgen. Aangezien het hier altijd een client zal betreffen waarover veel meer data zal worden verzameld, is het van belang voor alle data die over een client gaat, dezelfde URI te genereren. Dat zijn in de praktijk meestal betekenen dat een clientnummer gebruikt kan worden voor het maken van de URI. Als er data uit meerdere systemen komt, is het uiteraard van belang dat dezelfde identifier wordt gebruikt in die systemen.
### Ernst
De ernst van de wond, wordt uitgedrukt in één van vier mogelijke categorieën. Om deze te koppelen aan de wond, wordt een 'tussenstap' gemaakt om mogelijk te maken dat meerdere constateringen van de ernst kunnen worden gekoppeld, ieder met een unieke datum. Immers, de ernst van de wond zal zich ontwikkelen in de tijd. Hiervoor dient een instantie van de categorie [MedicalRatingQuality](http://purl.org/ozo/vph-g#MedicalRatingQuality) aangemaakt te worden. Omdat het niet noodzakelijk is deze ernst in zichzelf te kunnen identificeren, is het gebruikelijk hiervoor een zogenaamde 'blank node' aan te maken. Dat is een knooppunt in een graph zonder URI (ook wel een anonieme resource genoemd). Tools die gebruikt worden om RDF te genereren voorzien hierin. Voor deze stap hoeft dus geen URI te worden gegenereerd.
### Categorie
Aan de blank node voor de ernst wordt een categorie gekoppeld uit de klasse [Decubituscategorie](http://purl.org/ozo/vph-zorg#DecubitusCategorie). In dit voorbeeld categorie 2. Deze heeft de URI: http://purl.org/ozo/vph-zorg#decubitus_categorie_2

In de brondata zal deze URI niet voorkomen, dus deze moet worden samengesteld uit de informatie in de bron. Bijvoorbeeld door 'http://purl.org/ozo/vph-zorg#decubitus_categorie_' voor de categorie te plakken als deze aanwezig als een getal van 1 t/m 4. Wellicht is een andere tekstmanipulatie nodig, afhankelijk van het formaat van de brondata.
### turtle
Een voorbeeld van hoe de triples eruit kunnen zien in turtle formaat staat hieronder. De gebruikte URI's zijn omwille van duidelijkheid gekozen en zullen in de praktijk zeker een ander format hebben.
```
PREFIX vph-g: <http://purl.org/ozo/vph-g#>
PREFIX vph-zorg: <http://purl.org/ozo/vph-zorg#>
PREFIX ex: <http://data.example.com#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

ex:Jan a vph-g:Human ;
       vph-g:hasQuality ex:Wond .
ex:Wond a vph-zorg:DecubitusWond ;
        vph-g:inheresIn ex:Jan ;
        vph-zorg:herstelDatum "2021-12-05"^^xsd:date ;
        vph-g:hasQuality _ernst .
_ernst  a vph-g:MedicalRatingQuality ;
        vph-zorg:geconstateerdDatum "2021-11-16"^^xsd:date ;
        vph-g:hasQualityValue vph-zorg:decubitus_graad_2 .
```
# Voorbeeld brondata > RDF
Gegeven is de volgende brondata in de vorm van een .csv bestand.

|id|client_id|wond_id|datum|categorie|hersteldatum|
|----|----|----|----|----|----|
|1|1|1-1|2021-01-15|2|
|2|1|1-1|2021-01-18|3|2021-02-19|
|3|1|1-2|2021-08-03|3|
|4|2|2-1|2021-04-12|2|2021-06-01|
|5|3|3-1|2021-09-12|2|
|6|3|3-1|||2021-09-21|
|7|3|3-2|2021-11-11|3|

Hieronder worden een aantal voorbeelden uitgewekt, gebruik makend van verschillende tools, om deze data om te zetten naar RDF triples, conform de hierboven beschreven uitwerken conform de vph-zorg ontologie.
## OntoRefine
OntoRefine is een tool die meegeleverd wordt als onderdeel van de triplestore [GraphDB](https://graphdb.ontotext.com). De basis wordt gevormd door [OpenRefine](http://openrefine.org/). Een andere veelgebruikte variant is de [OpenRefine metadata extension](https://github.com/FAIRDataTeam/OpenRefine-metadata-extension), welke een vergelijkbare werking heeft.

Uitgebreide informatie over het gebruik van OntoRefine vindt u [hier](https://graphdb.ontotext.com/documentation/9.10/free/loading-data-using-ontorefine.html?highlight=ontorefine#). Naast een grafische interface is er ook een versie de gebruikt kan worden vanaf de command line.

![Configuratie](./img/Decubitus_ontorefine.png)
Hierboven ziet u het resultaat voor het gegeven CSV bestand.

De configuratie is op te slaan in json formaat. Resultaat vindt u [hier](./script/decubitus_mapping.json)

Door op de RDF knop te klikken, kunt u de RDF triples genereren en opslaan. Resultaat staat [hier](./data/decubitus-triples-ontorefine.ttl)
## RML
Rule Mapping Language (RML) is een extensie op [R2RML](https://www.w3.org/TR/r2rml/), de W2C standaard voor het converteren van data in relationele databases. RML werkt ook met andere bronnen, zoals CSV en JSON.

Een eenvoudige manier om RML of R2RML mappings te maken, is gebruik te maken van [YARRRML](https://rml.io/yarrrml/). Een extensie op YAML die bedoeld is voor betere mens-leesbaarheid. YARRML kan o.a. gemaakt worden door een online editer: [Matey](https://rml.io/yarrrml/matey/). Hieronder een voorbeeld voor de bewuste CSV.
```
prefixes:
  vph-g: "http://purl.org/ozo/vph-g#"
  vph-zorg: "http://purl.org/ozo/vph-zorg#"
  xsd: "http://www.w3.org/2001/XMLSchema#"
  data: "http://data.zorgaanbiederA.nl/"
mappings:
  wond:
    sources:
      - ['Decubitus.csv~csv']
    s: data:decubituswond/$(wond_id)
    po:
      - [a, vph-zorg:DecubitusWond]
      - [vph-g:inheresIn, data:client/$(client_id)]
      - [vph-zorg:herstelDatum, $(hersteldatum), xsd:date]
      - p: vph-g:hasQuality
        o: 
          v: $(id)
          type: blank
  ernst:
    sources:
      - ['Decubitus.csv~csv']
    s:
      v: $(id)
      type: blank
    po:
      - [a, vph-g:MedicalRatingQuality]
      - [vph-zorg:geconstateerdDatum, $(datum), xsd:date]
      - [vph-g:hasQualityValue, vph-zorg:decubitus_graad_$(categorie)]
```

De resulterende RML configuratie is te vinden in Source2RDF/script/decubitus_mapping.rml
Een paar opmerkigen over bovenstaande voorbeeld:
* Van de instantie voor de persoon die de wond heeft, wordt niet aangegeven dat deze van het type vph-g:Human is.
* Ook voor regels waarin geen decubitusgraad staat vermeld, wordt een blank node aangemaakt van het type vph-g:MedicalRatingQuality. Dat komt doordat de eerste kolom (record- of rijnummer) wordt gebruikt voor het genereren van de blank node. In het voorbeeld van OntoRefine wordt daar wat intelligenter mee omgegaan. Het kan geen kwaad dat er een blank node wordt aangemaak waar verder geen informatie aan toegevoegd wordt, maar het levert natuurlijk wel een extra triple zonder dat deze nut heeft.
## Pyhton
Door gebruik te maken van de library [rdflib](https://rdflib.dev) is het relatief eenvoudig om met RDF triples te werken in Python. De library wordt actief ontwikkeld. In dit document is het niet de bedoeling een handeling voor rdflib of Python te geven, daarover is genoeg te vinden op internet. Hieronder is een stukje code opgenomen ter inspiratie
```
from csv import reader
from rdflib import Graph, Namespace, URIRef, BNode, Literal, RDF, XSD

source = "Source2RDF/data/Decubitus.csv"
vph_g = Namespace('http://purl.org/ozo/vph-g#')
vph_zorg = Namespace('http://purl.org/ozo/vph-zorg#')
data = Namespace('http://data.zorgaanbiederA.nl/')

g = Graph()
#Open bronbestand
with open(source, 'r') as csv_file:
    csv_reader = reader(csv_file)
    #lees eerste rij als kolomnamen
    header = next(csv_reader)
    for row in csv_reader:
        if row[1] and row[2]: #client_id en wond_id zijn noodzakelijk
            client = URIRef(row[1], base=data + 'client/')
            wond = URIRef(row[2], base=data + 'wond/')
            g.add((client, RDF.type, vph_g.Human))
            g.add((wond, RDF.type, vph_zorg.DecubitusWond))
            g.add((wond, vph_g.inheresIn, client))
            if row[3] and row[4]: #graad en datum waarop deze geconstateerd is
                ernst = BNode()
                categorie = URIRef('http://purl.org/ozo/vph-zorg#decubitus_categorie_' + row[4])
                g.add((wond, vph_g.hasQuality, ernst))
                g.add((ernst, vph_g.hasQualityValue, categorie))
                g.add((ernst, vph_zorg.geconstateerdDatum, Literal(row[3], datatype=XSD.date)))
            if row[5]: #hersteldatum bekend
                g.add((wond, vph_zorg.hersteDatum, Literal(row[5], datatype=XSD.date)))

#sla de graph op in turtle formaat
g.serialize(destination='Source2RDF/data/decubitus-triples-python.ttl', format='ttl')
```
Resultaat staat [hier](./data/decubitus-triples-python.ttl)