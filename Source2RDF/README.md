Op deze plek zal aan de hand van praktische voorbeelden duidelijk worden gemaakt hoe brondata omgezet kan worden naar RDF triples die overeenkomen met de vph ontologie. Door de gegeven patronen te gebruiken werken de SPARQL queries voor alle brondata.
# Patronen
## Qualityvalues
Omdat eigenschappen van dingen (de ernst van een decubituswond, het gewicht van een client, etc.) vaak gekoppeld moeten worden aan extra informatie (de dag waarop de ernst is geconstateerd, de dag waarop een client is gewogen), wordt binnen de vph ontologie gewerkt met zogenaamde quality-regions. Dit houdt in dat in plaats van eigenschappen rechtstreeks te koppelen, een tussenstap wordt gemaakt. Op die manier kunnen meerdere instanties van een eigenschap worden gekoppeld, zonder dat onduidelijkheid ontstaat over bijvoorbeeld de meetdatum. Zie hieronder: welke datum hoort bij wel gewicht?

```mermaid
graph TD
    C{Jan} --> |gewicht|D[75]
    C --> |gewicht|E[68]
    C --> |meetdatum|F["2021-08-15"]
    C --> |meetdatum|G["2021-09-20"]
```
Gebruik tussenstap, zodat de informatie duidelijk gegroepeerd kan worden:

```mermaid
graph TD
    C{Jan}
    K{Kilo}
    C --> G1{_gewicht1}
    C --> G2{_gewicht2}
    G1 --> |gewicht|D[75]
    G2 --> |gewicht|E[68]
    G1 --> |meetdatum|F["2021-08-15"]
    G2 --> |meetdatum|G["2021-09-20"]
    G1 --> |eenheid|K
    G2 --> |eenheid|K
```
Het gebruikte patroon wordt uitgewerkt aan de hand van [decubituswonden](./Decubitus.md), waarbij de ernst van de wond uitgedrukt wordt in één van vier categorieën.
## Lokale kennis
Voor het klassificeren van bepaalde categorieën is vaak extra kennis nodig. Bijvoorbeeld: in een bronsysteem voor personeelsadministratie staat een tabel met contracten. In de ontologie wordt onderscheid gemaakt tussen verschillende soorten contracten, zoals arbeidscontracten, stage overeenkomsten, inhuur overeenkomsten, etc.

De vraag is op basis van welke kenmerken die in de tabel staan, kan worden geconcludeerd welk type contract het betreft. Er is bijvoorbeeld een contractcode, waarbij een contractcode van "005" betekent dat het hier om een stage overeenkomst gaat. In het voorbeeld over [werkovereenkomsten](./Werkovereenkomst.md) is een aantal manieren uitgewerkt hoe hiermee om te gaan.