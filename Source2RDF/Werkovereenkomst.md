# RDF triples m.b.t. Werkovereenkomst
Om een datastation te vullen met RDF triples, dient de brondata omgezet te worden naar dit formaat. In dit document worden een aantal manieren om dit te doen uitgewerkt, op basis van een minimale dataset over contracten.

| Contractnummer | Werknemer | contractcode | functiecode | functie_startdatum | functie_einddatum |
| - | - | - | - | - | - |
| 1 | Anne | 003 | 7135 | 2018-05-01 | - |

## RDF data
Op basis van de KIK-V ontologie kunnen de volgende triples worden gemaakt:
| subject | predicate | object |
| - | - | - |
| Contract_van_Anne | is_een | WerkOvereenkomst
| Contract_van_Anne | heeftOpdrachtnemer | Anne
| Contract_van_Anne | bijbehorendeFunctie | Functie_van_Anne
| Functie_van_Anne | is_een | Functie
| Functie_van_Anne | startDatum | "2018-05-01"
| Anne | is_een | Persoon
| Anne | heeftFunctie | Functie_van Anne

![Plaatje](./img/rdf_basis.png "Basis RDF triples")

Op basis van deze triples is nog niet te concluderen:
* Wat voor 'soort' Werkovereenkomst het Contract_van_Anne is (bijvoorbeeld een vrijwilligersovereenkomst of een stageovereenkomst)
* wat voor 'soort' functie de Functie_van_Anne is (bijvoorbeeld een managementfunctie of een ondersteunende functie)

Deze kennis is echter wel nodig om indicatoren te kunnen berekenen waarin onderscheid in soort overeenkomst en/of functie noodzakelijk is. De informatie om dit onderscheid te maken is wel aanwezig in de data (contractcode en functiecode in dit voorbeeeld). Hoe zorgen we ervoor dat deze kennis gebruikt kan worden voor het berekenen van indicatoren, zonder dat de dat zelf moet worden geconverteerd.

## Lokale kennis
De specifieke kennis voor de aanbieder van deze data moet worden ontsloten op één of andere manier. In dit voorbeeld gaan we uit van de volgende uitgangspunten:
* de RDF data blijft zoveel mogelijk gelijk aan de originele data
* de toegevoegde kennis heeft de vorm van een ontologie
* OWL-reasoning is beschikbaar

De kennis bestaat uit het een lijst met contractcodes en functiecodes en hun respectievelijke betekenis. Voor dit fictieve voorbeeld:
| contractcode | soort contract | 
|-|-|
|001|niet in gebruik|
|002|Arbeidsovereenkomst voor bepaalde tijd|
|003|Arbeidsovereenkomst voor onbepaalde tijd|

|functiecode|functietitel|functie categorie|
|-|-|-|
|7101|Verpleegkundige A|Zorgverlener|
|7135|Verpleegkundige B|Zorgverlener|
|8100|Teamhoofd|Management|

## Twee patronen
Deze kennis wordt verwerkt in een lokale OWL ontologie. Met de gegeven uitgangspunten kunnen twee oplossings-patronen worden gekozen:
1. equivalente klasse
2. subklasse

Beide patronen worden gebruikt om de kennis over contract resp. functie toe te voegen.

### equivalente klasse
Voor het patroon met gebruik van equivalente klassen worden de volgende stappen doorlopen in de lokale ontologie voor het onderscheid naar soort contract.
* importeren van relevante klassen uit KIK-V ontologieën:
    * vph-pers:ArbeidsovereenkomstBepaaldeTijd
    * vph-pers:ArbeidsovereenkomstOnbepaaldeTijd
* Aanmaken lokale variabele:
    * lokaal:contractcode
* Toevoegen kennis (axioma's)
    * vph-pers:ArbeidsovereenkomstBepaaldeTijd equivalent aan: Werkovereenkomst AND contractcode = '002'
    * vph-pers:ArbeidsovereenkomstOnbepaaldeTijd equivalent aan: Werkovereenkomst AND contractcode = '003'

### subklasse
Voor het patroon met gebruik van subklassen worden de volgende stappen doorlopen in de lokale ontologie voor het onderscheid naar soort functie.
* importeren van relevante klassen uit KIK-V ontologieën:
    * vph-pers:ZorgverlenerFunctie
    * vph-pers:ManagementFunctie
* Aamaken lokale klassen:
    * lokaal:Functie_7101 is subklasse van vph-pers:ZorgverlenerFunctie
    * lokaal:Functie_7135 is subklasse van vph-pers:ZorgverlenerFunctie
    * lokaal:Functie_8100 is subklasse van vph-pers:ManagementFunctie
* Toevoegen kennis (axioma's)
    * vph-pers:ZorgverlenerFunctie equivalent aan: Functie AND functiecode = '7101' OR '7135'
    * vph-pers:ManagementFunctie equivalent aan: Functie AND functiecode = '8100'

![Plaatje](./img/lokaal.png "lokale ontologie")
## RDF data o.b.v. lokale kennis
Met betrekking tot het contract, in combinatie met de equivalente klassen, wordt de volgende triple gemaakt:
| object | predicate | subject |
| - | - | - |
| Contract_van_Anne | lokaal:contractcode | '003'|

Op basis van deze data en de lokale kennis samen kan de volgende triple worden geconcludeerd (en daarmee gebruik worden in het correct berekenen van de indicatoren)
| object | predicate | subject |
| - | - | - |
| Contract_van_Anne | is_een | Contract voor onbepaalde tijd|

Met betrekking tot de functie, in combinatie met de gemaakte subklassen, wordt de volgende triple gemaakt:
| object | predicate | subject |
| - | - | - |
| Functie_van_Anne | is_een | lokaal:Functie_7135 |

En omdat de klasse lokaal:Functie_7135 een subklasse is van ZorgverlenerFunctie, kan de volgende triple worden geconcludeerd:
| object | predicate | subject |
| - | - | - |
| Functie_van_Anne | is_een | Zorgverlener functie |

Onderstaande figuur geeft een grafische weergave van dit voorbeeld. De RDF data die is aangemaakt staat in de rechthoek aan de onderkant. De geconcludeerde triples staan met een rode pijl weergegeven.
![Plaatje](./img/data_voorbeeld.png "Voorbeeld")

## Derde patroon
Naast de hierboven uitgewerkte patronen om de kennis over het soort contract of functie toe te voegen is er een derde (hoofd)patroon om dit te realiseren. Deze bestaat eruit om de kennis direct toe te voegen in het 'mechanisme' dat de oorspronkelijke data omzet in RDF triples.

Voor dit voorbeeld worden bij het derde patroon twee verschillende mechanismen toegelicht.
1. (deel)selectie aan de bron
2. programmeren ETL script

### (deel)select aan de bron
Hiermee wordt bedoeld dat voor het omzetten naar RDF triples niet de volledige bron (bijvoorbeeld tabel met contracten), maar alleen specifieke delen worden geselecteerd. Voor die selectie wordt dan de vereiste kennis direct toegevoegd.

Je zou er bij de contracten bijvoorbeeld voor kunnen kiezen om alleen de contracten te selecteren met contractcode '002'. Van al die contracten kan dan aangegeven worden dat ze van het type ArbeidsovereenkomstBepaaldeTijd zijn. Daarmee kan het algemene type WerkOvereenkomst worden overgeslagen zoals bovenaan genoemd is onder de eerste stap.

Vervolgens kan een selectie gemaakt worden voor de contracten met contractcode '003', om deze te typeren als ArbeidsovereenkomstOnbepaaldeTijd. En zo verder voor alle soorten WerkOvereenkomsten.

Afhankelijk van de hoeveelheid klassen en de manier waarop de data is geordend (bijvoorbeeld een aparte tabel of applicatie met alle stage-overenkomsten) is deze methode voor de hand liggend of misschien erg bewerkbaar.
### programmeren ETL script
Het omzetten naar RDF triples kan uitgevoerd worden middels verschillende tools. Er kan ook gekozen worden voor het gebruik van een programeertaal zoals Python. Vaak zijn specifieke libraries beschikbaar (bijvoorbeeld rdflib) voor het werken met RDF data.

In dat geval kan de kennis over contractcodes eenvoudigweg worden geprogrammeerd:
```python
#hypothetische python code
import rdflib

if contractcode == '002':
    addTriple(contract, RDF.type, vph-pers:ArbeidsovereenkomstBepaaldeTijd)
elif contractcode == '003':
    addTriple(contract, RDF.type, vph-pers:ArbeidsovereenkomstOnbepaaldeTijd)
else:
    addTriple(contract, RDF.type, vph-pers:WerkOvereenkomst)
```
Het overduidelijke nadeel van deze laatste methode is dat de kennis over contracten 'verstopt' zit in code en dat de onderhoudbaarheid van de kennis wellicht lastiger is.
