# Vph-zorg ontologie in de praktijk
Voorbeeld over hoe de vph-zorg ontologie beoogt om te gaan met het soort zorg dat geleverd wordt aan een client en tot welke typering van de organisatie (en haar vestigingen) dit logischerwijs zou kunnen leiden.

Uitgangspunt is hierbij steeds dat vaststaande 'soorten typeringen' vastliggen in bijvoorbeeld wet- of regelgeving. In dit geval m.n. de Wet Langdurige Zorg.

![Verpleegproces](./img/Verpleegproces.png "Grafische weergave deel vph-zorg ontologie")
Grafische weergave deel vph-zorg ontologie

## Voorbeeld indiceren en leveren van zorg
* Ina heeft een Wlz indicatie voor het zorgprofiel 4VV: 'Beschut wonen met intensieve begeleiding en uitgebreide verzorging'. De leveringsvorm is 'Verblijf in instelling'.
* Ina ontvangt zorg op locatie 'Vestiging_A', van verpleeghuisorganisatie 'Organisatie_X'.
* De zorg die zijn ontvangt is conform het zorgprofiel 5VV: 'Beschermd wonen met intensieve verzorging en verpleging' en de leveringsvorm is 'Verblijf in instelling'.

Hieronder staan deze feiten weergegeven in RDF triples


```
prefix : <http://example.com/>
prefix vph-g: <purl.org/ozo/vph-g#>
prefix vph-zorg: <purl.org/ozo/vph-zorg#>
prefix vph-org: <purl.org/ozo/vph-org#>

:indicatieVanIna a vph-zorg:WlzIndicatie ;
                 vph-g:isAbout :Ina ;
                 vph-g:hasPart vph-zorg:4VV ;
                 vph-g:hasPart vph-zorg:instelling ;

:VerpleegprocesVanIna a vph-zorg:NursingProcess ;
                      vph-g:hasParticipant :Ina ;
                      vph-g:hasPerdurantLocation :Vestiging_A ;
                      vph-g:describedBy vph-zorg:5VV ;
                      vph-g:describedBy vph-zorg:instelling .

:VestigingA a vph-org:Vestiging ;
            vph-org:pisVestigingVan :Organisatie_X ;
```

De conclusies die op basis van deze gegevens getrokken kunnen worden over de verpleeghuisinstelling.

* Vestiging_A levert zorg in de sector Verpleging en Verzorging
* Vestiging_A levert zorg conform profiel 5VV
* Organisatie_X levert zorg in de vorm van 'Verblijf in instelling'