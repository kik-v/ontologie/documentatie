from csv import reader
from rdflib import Graph, Namespace, URIRef, BNode, Literal, RDF, XSD

source = "Source2RDF/data/Decubitus.csv"
vph_g = Namespace('http://purl.org/ozo/vph-g#')
vph_zorg = Namespace('http://purl.org/ozo/vph-zorg#')
data = Namespace('http://data.zorgaanbiederA.nl/')

g = Graph()
#Open bronbestand
with open(source, 'r') as csv_file:
    csv_reader = reader(csv_file)
    #lees eerste rij als kolomnamen
    header = next(csv_reader)
    for row in csv_reader:
        if row[1] and row[2]: #client_id en wond_id zijn noodzakelijk
            client = URIRef(row[1], base=data + 'client/')
            wond = URIRef(row[2], base=data + 'wond/')
            g.add((client, RDF.type, vph_g.Human))
            g.add((wond, RDF.type, vph_zorg.DecubitusWond))
            g.add((wond, vph_g.inheresIn, client))
            if row[3] and row[4]: #graad en datum waarop deze geconstateerd is
                ernst = BNode()
                categorie = URIRef('http://purl.org/ozo/vph-zorg#decubitus_categorie_' + row[4])
                g.add((wond, vph_g.hasQuality, ernst))
                g.add((ernst, vph_g.hasQualityValue, categorie))
                g.add((ernst, vph_zorg.geconstateerdDatum, Literal(row[3], datatype=XSD.date)))
            if row[5]: #hersteldatum bekend
                g.add((wond, vph_zorg.hersteDatum, Literal(row[5], datatype=XSD.date)))

#sla de graph op in turtle formaat
g.serialize(destination='Source2RDF/data/decubitus-triples-python.ttl', format='ttl')